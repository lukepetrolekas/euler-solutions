/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eulersolutions;

import eulersolutions.solution.*;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author aurix
 */
public class EulerSolutions {

    public static final HashMap<Integer, Problem> db = new HashMap<>();

    static {
        db.put(1, new Problem0001());
        db.put(2, new Problem0002());
        db.put(3, new Problem0003());
        db.put(4, new Problem0004());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // create a scanner so we can read the command-line input
        Scanner scanner = new Scanner(System.in);

        while (true) {

            //  prompt for the number
            System.out.print("Enter problem number: ");

            int n;
            // get the number
            if (scanner.hasNextInt()) {
                n = scanner.nextInt();
            } else {
                System.out.print("Quit");
                return;
            }

            System.out.println(String.format("Solving problem #%d", n));

            db.get(n).solve();
        }

    }

}
