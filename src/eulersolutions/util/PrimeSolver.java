/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eulersolutions.util;

public final class PrimeSolver {

    private static boolean isPrimeRepeatedDivision(long n) {
        if (n < 2) {
            return false;
        }
        
        long sqrt2 = (long) Math.sqrt(n);

        for (long i = 2; i <= sqrt2; i++) {
            if (n % i == 0) {
                return false;
            }
        }

        return true;
    }

    public enum Strategy {
        REPEATED_DIVISION
    }

    public static final boolean isPrime(long i, Strategy s) {
        switch (s) {
            case REPEATED_DIVISION:
                return isPrimeRepeatedDivision(i);
            default:
                return isPrimeRepeatedDivision(i);
        }
    }
}
