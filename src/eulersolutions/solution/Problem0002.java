/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eulersolutions.solution;

import eulersolutions.Problem;

/**
 *
 * @author aurix
 */
public class Problem0002 implements Problem {

    @Override
    public void solve() {
        int sum = 0;
        int i = 1;
        int i_prev = 1;
        
        while (i <= 4000000) {
            if (i % 2 == 0)
                sum += i;
                       
            i = i_prev + i;
            i_prev = i - i_prev;
        }
        
        System.out.println(sum);
    }
    
}
