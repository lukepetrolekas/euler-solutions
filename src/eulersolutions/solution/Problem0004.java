/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eulersolutions.solution;

import eulersolutions.Problem;
import eulersolutions.util.StringUtil;

/**
 *
 * @author aurix
 */
public class Problem0004 implements Problem {

    @Override
    public void solve() {
        int max = 0;
        for (int a = 999; a > 1; a--) {
            for (int b = a; b > 1; b--) {
                String s = String.valueOf(a * b);
                if (s.equals(StringUtil.flip(s))) {
                    max = Math.max(max, a*b);
                }
            }
        }
        
        System.out.println(max);
    }

}
