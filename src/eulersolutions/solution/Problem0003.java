/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eulersolutions.solution;

import eulersolutions.Problem;
import eulersolutions.util.PrimeSolver;

/**
 *
 * @author aurix
 */
public class Problem0003 implements Problem{
    
    @Override
    public void solve() {
        // Start at where the highest prime will be (at maximum)
        for (long i = (long) Math.sqrt(600851475143L); i > 1; i--){
            if (600851475143L % i == 0 && 
                PrimeSolver.isPrime(i, PrimeSolver.Strategy.REPEATED_DIVISION))
            {
                System.out.println(i);
                return;
            }
        }
        
        System.out.println(600851475143L); // "should this number be prime"
    }
}
